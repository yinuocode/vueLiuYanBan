import Vue from 'vue'
import Router from 'vue-router'
import VueResource from 'vue-resource';

import vHeader from '@/components/header/header'
import vFooter from '@/components/footer/footer'
import messages from '@/components/messages/messages'

import '@/common/css/mygbook.css'

Vue.use(Router);
Vue.use(VueResource);

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/messages'
    }, {
      path: '/messages',
      name: 'messages',
      component: messages
    }, {
      path: '/header',
      name: 'vHeader',
      component: vHeader
    }, {
      path: '/footer',
      name: 'vFooter',
      component: vFooter
    }
  ]
})
