// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  extends: 'standard',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // add your custom rules here
  // 摆脱令人抓狂的ESlint 语法检测配置
  /*
    "off"或者0    //关闭规则关闭
    "warn"或者1    //在打开的规则作为警告（不影响退出代码）
    "error"或者2    //把规则作为一个错误（退出代码触发时为1）
  */
  'rules': {
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    // 新增
    // 'semi': ['error', 'always'],  //加上会报错
    // 关闭语句强制分号结尾
    "semi": 0,
    // js属性可以使用双引号 引号类型 `` "" ''
    'quotes': 0,
    // 对象字面量中的属性名是否强制双引号
    'quote-props': 0,
    "comma-dangle": 0, // 对象字面量项尾不能有逗号 [2, "never"]
    "no-irregular-whitespace": 0, // 不能有不规则的空格
    "no-multi-spaces": 0, // 不能用多余的空格
    "no-multiple-empty-lines": 0, // [1, {"max": 2}],//空行最多不能超过2行
    'no-tabs': 0,
    'indent': 0,
    'space-before-function-paren': 0
  }
}
